import { MenuFoldOutlined, MenuUnfoldOutlined } from "@ant-design/icons";
import React from "react";
import { Breadcrumb } from "antd";
function Top(props) {
  let { collapsed, setCollapsed } = props;
  return (
    <>
      <div className="left">
        {React.createElement(
          collapsed ? MenuUnfoldOutlined : MenuFoldOutlined,
          {
            className: "trigger",
            onClick: () => setCollapsed(!collapsed),
          }
        )}
        <Breadcrumb>
          <Breadcrumb.Item>后台系统</Breadcrumb.Item>
          <Breadcrumb.Item>首页</Breadcrumb.Item>
          <Breadcrumb.Item>用户列表</Breadcrumb.Item>
          <Breadcrumb.Item></Breadcrumb.Item>
        </Breadcrumb>
      </div>
      <div className="right">杀马特团长（金色闪光）</div>
    </>
  );
}
export default Top;
