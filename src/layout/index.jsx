import { Container } from "./styled";
import {
  UserOutlined,
  VideoCameraOutlined,
  SettingOutlined,
} from "@ant-design/icons";
import { Layout, Menu } from "antd";
import Top from "./components/top";
import React, { useState } from "react";
import { useNavigate, Outlet } from "react-router-dom";
const { Header, Sider, Content } = Layout;

const LayoutConponent = () => {
  const [collapsed, setCollapsed] = useState(false);
  let navigate = useNavigate();
  return (
    <Container>
      <Layout>
        <Sider trigger={null} collapsible collapsed={collapsed}>
          <div className="logo" />
          <Menu
            onClick={({ key, keyPath, domEvent }) => {
              console.log(key, keyPath, domEvent);
              let url = {
                "1-1": "/admin",
                "1-2": "/history",
                "2-1": "/admin/users",
                "2-2": "/admin/users/create",
                "3-1": "/admin/orders",
                "3-2": "/admin/orders/total",
              };
              navigate(url[key]);
            }}
            theme="dark"
            mode="inline"
            defaultSelectedKeys={["1"]}
            items={[
              {
                key: "1",
                icon: <UserOutlined />,
                label: "首页",
                children: [
                  {
                    key: "1-1",
                    icon: <SettingOutlined />,
                    label: "欢迎页",
                  },
                  {
                    key: "1-2",
                    icon: <SettingOutlined />,
                    label: "访客记录",
                  },
                ],
              },
              {
                key: "2",
                icon: <VideoCameraOutlined />,
                label: "用户管理",
                children: [
                  {
                    key: "2-1",
                    icon: <SettingOutlined />,
                    label: "用户列表",
                  },
                  {
                    key: "2-2",
                    icon: <SettingOutlined />,
                    label: "用户创建",
                  },
                ],
              },
              {
                key: "3",
                icon: <VideoCameraOutlined />,
                label: "订单管理",
                children: [
                  {
                    key: "3-1",
                    icon: <SettingOutlined />,
                    label: "订单管理",
                  },
                  {
                    key: "3-2",
                    icon: <SettingOutlined />,
                    label: "订单统计",
                  },
                ],
              },
            ]}
          />
        </Sider>
        <Layout className="site-layout">
          <Header
            className="site-layout-background"
            style={{
              padding: 0,
            }}
          >
            <Top setCollapsed={setCollapsed} collapsed={collapsed}></Top>
          </Header>
          <Content
            className="site-layout-background"
            style={{
              margin: "24px 16px",
              padding: 24,
              minHeight: 280,
              overflowY: "scroll",
            }}
          >
            <Outlet></Outlet>
          </Content>
        </Layout>
      </Layout>
    </Container>
  );
};

export default LayoutConponent;
