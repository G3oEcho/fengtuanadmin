import styled from "styled-components";

export const Container = styled.div`
  height: 100%;
  header {
    background: #fff;
    display: flex;
    align-items: center;
  }

  .left {
    width: 70%;
    height: 64px;
    display: flex;
    justify-content: flex-start;
    align-items: center;
    .anticon {
      width: 50px;
      text-align: center;
    }
  }
  .right {
    width: 30%;
    height: 64px;
    display: flex;
    justify-content: flex-end;
  }

  .ant-layout {
    height: 100%;
  }
`;
