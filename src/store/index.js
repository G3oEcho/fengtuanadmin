// import { combineReducers, createStore } from "redux";
import { createStore, applyMiddleware } from "redux";
import { combineReducers } from "redux-immutable";
import thunk from "redux-thunk";

import users from "../pages/users/store";

export default createStore(
  combineReducers({
    // login: reducer,
    // order: reducer,
    users,
  }),
  applyMiddleware(thunk)
);
