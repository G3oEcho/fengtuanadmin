import request from "../utils/request";
import qs from "qs";

export const postLoginApi = (params) => {
  return request({
    url: "users/login.jsp",
    method: "post",
    data: qs.stringify(params),
  });
};

// export const post功能api = (params) => {
//   return request({
//     url: "sms/login.jsp",
//     method: "post",
//     data: qs.stringify(params),
//   });
// };
