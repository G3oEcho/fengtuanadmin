import request from "../utils/request";
import qs from "qs";
export const getUsersApi = (params) => {
  return request({
    url: "users/index.jsp",
    method: "get",
    params: qs.stringify(params),
  });
};
export const CreateUsersApi = (params) => {
  return request({
    url: "users/create.jsp",
    method: "post",
    data: qs.stringify(params),
  });
};
export const DelUsersApi = (params) => {
  return request({
    url: "users/delete.jsp",
    method: "delete",
    params: qs.stringify(params),
  });
};
export const UpdateUsersApi = (params) => {
  return request({
    url: "users/update.jsp",
    method: "put",
    data: qs.stringify(params),
  });
};
