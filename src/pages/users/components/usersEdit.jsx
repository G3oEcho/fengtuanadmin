import { Modal } from "antd";
import { Button, Input, Form, Select, message } from "antd";
import { UpdateUsersApi } from "../../../api/users";
const { Option } = Select;
function UsersEdit(props) {
  const { isModalVisible, setIsModalVisible, handleInitData, params } = props;
  console.log(props);

  const handleCancel = () => {
    setIsModalVisible(false);
  };
  //表单登陆
  const onFinish = async (values) => {
    console.log("Success:", values);
    values.user_id = props.editRow.user_id;
    values.username = values.uname;
    let res = await UpdateUsersApi(values);
    console.log(res);
    if ((res.meta.state = 200)) {
      message.success("更新成功");
      setIsModalVisible(false);
      handleInitData(params);
    }
  };

  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };
  const validateLimit = (_, value) => {
    if (!value) {
      return Promise.reject(new Error("请输入密保问题"));
    }
    if (value.length < 2 || value.length > 10) {
      return Promise.reject(new Error("密码至少2~10位"));
    } else {
      return Promise.resolve();
    }
  };

  return (
    <Modal
      title="编辑用户"
      destroyOnClose={true}
      visible={isModalVisible}
      onCancel={handleCancel}
      footer={null}
    >
      {/* 表单 */}
      <Form
        size="large"
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
        initialValues={{
          uname: props.editRow.username,
          question: props.editRow.passwd_question,
          answer: props.editRow.passwd_answer,
          mobile: props.editRow.mobile,
        }}
      >
        <Form.Item
          name="question"
          rules={[{ required: true, message: "请选择密保问题" }]}
        >
          <Select placeholder="请选择你的密保问题">
            <Option value="您其中一位老师的名字">您其中一位老师的名字</Option>
            <Option value="你初恋的名字">你初恋的名字</Option>
            <Option value="你前女友的名字">你前女友的名字</Option>
          </Select>
        </Form.Item>
        <Form.Item
          name="answer"
          validateTrigger="onBlur"
          rules={[{ validator: validateLimit }]}
        >
          <Input placeholder="请输入你的密保答案" />
        </Form.Item>
        <Form.Item
          name="uname"
          validateTrigger="onBlur"
          rules={[
            {
              validator: (_, value) => {
                if (!value) {
                  return Promise.reject(new Error("请输入用户名"));
                } else if (value.length < 2 || value.length > 10) {
                  return Promise.reject(new Error("请输入2~10个字符"));
                } else {
                  return Promise.resolve();
                }
              },
            },
          ]}
        >
          <Input placeholder="请输入你的用户名" />
        </Form.Item>
        <Form.Item
          name="password"
          validateTrigger="onBlur"
          rules={[
            {
              validator: (_, value) => {
                if (value) {
                  if (value.length < 2 || value.length > 10) {
                    return Promise.reject(new Error("请输入2~10个字符"));
                  } else {
                    return Promise.resolve();
                  }
                } else {
                  return Promise.resolve();
                }
              },
            },
          ]}
        >
          <Input placeholder="请输入你的密码" />
        </Form.Item>
        <Form.Item
          name="mobile"
          validateTrigger="onBlur"
          rules={[
            {
              validator: (_, value) => {
                if (!value) {
                  return Promise.reject(new Error("请输入手机号"));
                } else if (!/^1\d{10}$/.test(value)) {
                  return Promise.reject(new Error("请输入正确的手机号"));
                } else {
                  return Promise.resolve();
                }
              },
            },
          ]}
        >
          <Input placeholder="请输入你的手机号" />
        </Form.Item>
        <Button type="primary" htmlType="submit">
          更新
        </Button>
      </Form>
    </Modal>
  );
}
export default UsersEdit;
