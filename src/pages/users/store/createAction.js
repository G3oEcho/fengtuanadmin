import { getUsersApi } from "../../../api/users";

export const getUsersAction = (parmas) => {
  return async (dispatch) => {
    let res = await getUsersApi(parmas);
    res.data.total = parseInt(res.data.total);
    console.log(res);
    dispatch({
      type: "USERS/SET_TABLEDATA",
      payload: res.data,
    });
  };
};
