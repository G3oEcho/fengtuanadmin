//导入样式
import { Container } from "./styled";

//导入api
import { useNavigate } from "react-router-dom";
import { CreateUsersApi } from "../../api/users";
//导入组件
import { Card, Button, Input, Form, Select, message } from "antd";
const { Option } = Select;

function Users() {
  //路由
  const navigate = useNavigate();
  //表单登陆
  const onFinish = async (values) => {
    console.log("Success:", values);
    let res = await CreateUsersApi(values);
    console.log(res);
    if (res.meta.state === 201) {
      message.success("创建成功");
      navigate("/admin/users");
    } else {
      message.error(res.meta.msg);
    }
  };

  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };
  const validateLimit = (_, value) => {
    if (!value) {
      return Promise.reject(new Error("请输入密保问题"));
    }
    if (value.length < 2 || value.length > 10) {
      return Promise.reject(new Error("密码至少2~10位"));
    } else {
      return Promise.resolve();
    }
  };
  return (
    <Container>
      <Card
        title="用户列表"
        extra={
          <Button type="primary" href="/admin/users">
            返回
          </Button>
        }
      >
        {/* 表单 */}
        <Form size="large" onFinish={onFinish} onFinishFailed={onFinishFailed}>
          <Form.Item
            name="question"
            rules={[{ required: true, message: "请选择密保问题" }]}
          >
            <Select placeholder="请选择你的密保问题">
              <Option value="您其中一位老师的名字">您其中一位老师的名字</Option>
              <Option value="你初恋的名字">你初恋的名字</Option>
              <Option value="你前女友的名字">你前女友的名字</Option>
            </Select>
          </Form.Item>
          <Form.Item
            name="answer"
            validateTrigger="onBlur"
            rules={[{ validator: validateLimit }]}
          >
            <Input placeholder="请输入你的密保答案" />
          </Form.Item>
          <Form.Item
            name="username"
            validateTrigger="onBlur"
            rules={[
              {
                validator: (_, value) => {
                  if (!value) {
                    return Promise.reject(new Error("请输入用户名"));
                  } else if (value.length < 2 || value.length > 10) {
                    return Promise.reject(new Error("请输入2~10个字符"));
                  } else {
                    return Promise.resolve();
                  }
                },
              },
            ]}
          >
            <Input placeholder="请输入你的用户名" />
          </Form.Item>
          <Form.Item
            name="password"
            validateTrigger="onBlur"
            rules={[
              {
                validator: (_, value) => {
                  if (!value) {
                    return Promise.reject(new Error("请输入密码"));
                  } else if (value.length < 2 || value.length > 10) {
                    return Promise.reject(new Error("请输入2~10个字符"));
                  } else {
                    return Promise.resolve();
                  }
                },
              },
            ]}
          >
            <Input placeholder="请输入你的密码" />
          </Form.Item>
          <Form.Item
            name="mobile"
            validateTrigger="onBlur"
            rules={[
              {
                validator: (_, value) => {
                  if (!value) {
                    return Promise.reject(new Error("请输入手机号"));
                  } else if (!/^1\d{10}$/.test(value)) {
                    return Promise.reject(new Error("请输入正确的手机号"));
                  } else {
                    return Promise.resolve();
                  }
                },
              },
            ]}
          >
            <Input placeholder="请输入你的手机号" />
          </Form.Item>
          <Button type="primary" htmlType="submit">
            创建
          </Button>
        </Form>
      </Card>
    </Container>
  );
}
export default Users;
