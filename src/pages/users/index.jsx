//导入样式
import { Container } from "./styled";

//导入api
import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import { getUsersAction } from "./store/createAction";
//导入组件
import {
  FormOutlined,
  DeleteOutlined,
  ExclamationCircleOutlined,
} from "@ant-design/icons";
import {
  Card,
  Button,
  Input,
  DatePicker,
  Space,
  Table,
  Pagination,
  Modal,
  message,
} from "antd";
import UsersEdit from "./components/usersEdit";
import { DelUsersApi } from "../../api/users";
const { Search } = Input;
const { RangePicker } = DatePicker;

function Users(props) {
  let [params, setParmas] = useState({
    pagenum: 1,
    pagesize: 10,
  });
  useEffect(() => {
    props.handleInitData(params);
    // eslint-disable-next-line
  }, [params]);
  const onSearch = (value) => {
    console.log(value);
  };
  const handleDel = (record) => {
    console.log(record);
    Modal.confirm({
      title: "提示",
      icon: <ExclamationCircleOutlined />,
      content: "是否确定删除",
      async onOk() {
        let res = await DelUsersApi({ user_id: record.user_id });
        props.handleInitData(params);
        message.success(res.meta.msg);
      },
      onCancel() {
        message.error("操作已取消");
      },
    });
  };
  const onChange = (value, dateString) => {
    console.log("Selected Time: ", value);
    console.log("Formatted Selected Time: ", dateString);
  };

  const onOk = (value) => {
    console.log("onOk: ", value);
  };
  const onPage = (pagenum, pagesize) => {
    setParmas({
      ...params,
      pagenum,
    });
  };
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [editRow, setEditRow] = useState({});
  const columns = [
    {
      title: "编号",
      dataIndex: "user_id",
      key: "user_id",
      width: 180,
    },
    {
      title: "用户",
      dataIndex: "username",
      key: "username",
      width: 180,
    },
    {
      title: "角色",
      dataIndex: "role_name",
      key: "role_name",
      width: 180,
    },
    {
      title: "电话",
      dataIndex: "mobile",
      key: "mobile",
      width: 180,
    },
    {
      title: "冻结",
      dataIndex: "state",
      key: "state",
      width: 180,
    },
    {
      title: "Action",
      key: "operation",
      fixed: "right",
      width: 150,
      render: (text, record, index) => {
        return (
          <>
            <Button
              onClick={() => {
                console.log(record);
                setEditRow(record);
                setIsModalVisible(true);
              }}
            >
              <FormOutlined />
            </Button>
            &emsp;
            <Button
              onClick={() => {
                handleDel(record);
              }}
            >
              <DeleteOutlined />
            </Button>
          </>
        );
      },
    },
  ];
  return (
    <Container>
      {/* 编辑组件 */}

      <Card
        title="用户列表"
        extra={
          <Button type="primary" href="/admin/users/create">
            创建
          </Button>
        }
      >
        <UsersEdit
          params={params}
          handleInitData={props.handleInitData}
          isModalVisible={isModalVisible}
          setIsModalVisible={setIsModalVisible}
          editRow={editRow}
        ></UsersEdit>
        <Search
          placeholder="input search text"
          allowClear
          size="large"
          onSearch={onSearch}
          style={{
            width: 304,
          }}
        />
        <Space direction="vertical">
          <RangePicker
            size="large"
            showTime={{
              format: "HH:mm",
            }}
            format="YYYY-MM-DD HH:mm"
            onChange={onChange}
            onOk={onOk}
          />
        </Space>
        {/* 表格 */}
        <Table
          dataSource={props.tableData.list}
          columns={columns}
          pagination={false}
          rowKey={"user_id"}
        />
        {/* 分页 */}
        <Pagination
          total={85}
          showSizeChanger
          showQuickJumper
          onChange={onPage}
          showTotal={(total) => `Total ${total} items`}
        />
      </Card>
    </Container>
  );
}
const mapStateToProps = (state) => {
  console.log(state.toJS());
  return {
    tableData: state.toJS().users.tableData,
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    handleInitData: (params) => dispatch(getUsersAction(params)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Users);
