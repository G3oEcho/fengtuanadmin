//跳转
import { useNavigate } from "react-router-dom";
import { postLoginApi } from "../../api/login";
//导入styled
import { Container } from "./styled";

//导入antd
import { Form, Input, Select, Button, message } from "antd";
const { Option } = Select;

function Login() {
  //路由
  const navigate = useNavigate();
  //表单登陆
  const onFinish = async (values) => {
    let res = await postLoginApi(values);
    if (res.meta.state === 200) {
      localStorage.setItem("token", res.data.token);
      localStorage.setItem("uname", res.data.uname);
      localStorage.setItem("roleName", res.data.roleName);
      message.success("登陆成功");
      navigate("/admin");
    } else {
      message.error(res.meta.msg);
    }
  };

  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };

  const validateLimit = (_, value) => {
    if (!value) {
      return Promise.reject(new Error("请输入密保问题"));
    }
    if (value.length < 2 || value.length > 10) {
      return Promise.reject(new Error("密码至少2~10位"));
    } else {
      return Promise.resolve();
    }
  };
  return (
    <Container>
      <Form size="large" onFinish={onFinish} onFinishFailed={onFinishFailed}>
        <h1 className="title">锋团管理系统</h1>
        <Form.Item
          name="question"
          rules={[{ required: true, message: "请选择密保问题" }]}
        >
          <Select placeholder="请选择你的密保问题">
            <Option value="您其中一位老师的名字">您其中一位老师的名字</Option>
            <Option value="你初恋的名字">你初恋的名字</Option>
            <Option value="你前女友的名字">你前女友的名字</Option>
          </Select>
        </Form.Item>
        <Form.Item
          name="answer"
          validateTrigger="onBlur"
          rules={[{ validator: validateLimit }]}
        >
          <Input placeholder="请输入你的密保答案" />
        </Form.Item>
        <Form.Item
          name="uname"
          validateTrigger="onBlur"
          rules={[
            {
              validator: (_, value) => {
                if (!value) {
                  return Promise.reject(new Error("请输入用户名"));
                } else if (value.length < 2 || value.length > 10) {
                  return Promise.reject(new Error("请输入2~10个字符"));
                } else {
                  return Promise.resolve();
                }
              },
            },
          ]}
        >
          <Input placeholder="请输入你的用户名" />
        </Form.Item>
        <Form.Item
          name="pwd"
          validateTrigger="onBlur"
          rules={[
            {
              validator: (_, value) => {
                if (!value) {
                  return Promise.reject(new Error("请输入密码"));
                } else if (value.length < 2 || value.length > 10) {
                  return Promise.reject(new Error("请输入2~10个字符"));
                } else {
                  return Promise.resolve();
                }
              },
            },
          ]}
        >
          <Input placeholder="请输入你的密码" />
        </Form.Item>
        <Button type="primary" htmlType="submit">
          登陆
        </Button>
      </Form>
    </Container>
  );
}

export default Login;
