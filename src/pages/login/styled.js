import styled from "styled-components";
export const Container = styled.div`
  width: 100%;
  height: 100%;
  background-color: #2d3a4b;

  display: flex;
  justify-content: center;
  align-items: center;
  .title {
    color: white;
    font-size: 40px;
    text-align: center;
    margin-bottom: 10px;
  }
  .ant-form {
    width: 300px;
  }
  .ant-btn {
    width: 300px;
  }
`;
