import styled from "styled-components";

export const Container = styled.div`
  .total {
    display: flex;
    justify-content: space-between;
  }
  .box {
    width: 22%;
    box-shadow: -3px -4px 5px #ccc;
    padding: 10px;
    border-radius: 10px;
    color: white;
  }
  .order_num {
    font-size: 30px;
    font-weight: bold;
  }
  .box:first-child {
    background: #db5995;
  }
  .box:nth-child(2) {
    background: #6dc894;
  }
  .box:nth-child(3) {
    background: #cfbd83;
  }
  .box:nth-child(4) {
    background: #4d9ec5;
  }
  .middle {
    width: 96%;
    height: 300px;
    box-shadow: 0 0 5px 1px #ccc;
    margin: 20px;
    /* display: flex;
    justify-content: center;
    align-items: center; */
  }
`;
