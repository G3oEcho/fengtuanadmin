import { useEffect, useRef } from "react";
import * as echarts from "echarts";

function LineBarChart() {
  const chartRef = useRef(null);

  useEffect(() => {
    let chartInstance = echarts.init(chartRef.current);
    const option = {
      xAxis: {
        type: "category",
        data: ["Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"],
      },
      yAxis: {
        type: "value",
      },
      series: [
        {
          data: [150, 230, 224, 218, 135, 147, 260],
          type: "line",
        },
      ],
    };
    chartInstance.setOption(option);
  }, []);

  return <div ref={chartRef} style={{ height: "400px" }}></div>;
}

export default LineBarChart;
