//导入样式
import { Container } from "./styled";
//导入api

//导入组件
import {
  Button,
  Card,
  Input,
  DatePicker,
  Space,
  Table,
  Pagination,
} from "antd";
import {
  SearchOutlined,
  FormOutlined,
  DeleteOutlined,
} from "@ant-design/icons";
const { RangePicker } = DatePicker;
//table数据
const columns = [
  {
    title: "姓名",
    dataIndex: "name",
    key: "name",
  },
  {
    title: "年龄",
    dataIndex: "age",
    key: "age",
  },
  {
    title: "住址",
    dataIndex: "address",
    key: "address",
  },
  {
    title: "Action",
    key: "operation",
    fixed: "right",
    width: 200,
    render: () => (
      <>
        <Button>
          <FormOutlined />
        </Button>
        &emsp;
        <Button>
          <DeleteOutlined />
        </Button>
      </>
    ),
  },
];
const dataSource = [
  {
    key: "1",
    name: "胡彦斌",
    age: 32,
    address: "西湖区湖底公园1号",
  },
  {
    key: "2",
    name: "胡彦祖",
    age: 42,
    address: "西湖区湖底公园1号",
  },
];
function Orders() {
  //日期搜索
  const onChange = (value, dateString) => {
    console.log("Selected Time: ", value);
    console.log("Formatted Selected Time: ", dateString);
  };

  const onOk = (value) => {
    console.log("onOk: ", value);
  };
  const onPage = (pagenum, pagesize) => {
    console.log(pagenum, pagesize);
  };
  return (
    <Container>
      <Card
        title="订单列表"
        extra={
          <Button type="primary" shape="round">
            导出数据
          </Button>
        }
      >
        <div className="title">
          <Input.Group compact>
            <Input
              placeholder="订单编号"
              style={{ width: "200px" }}
              size="large"
            />
            <Input
              placeholder="下单人"
              style={{ width: "200px" }}
              size="large"
            />
            <Input
              placeholder="收货人"
              style={{ width: "150px" }}
              size="large"
            />
            <Button icon={<SearchOutlined />} size="large"></Button>
          </Input.Group>
          <Space direction="vertical">
            <RangePicker
              size="large"
              showTime={{
                format: "HH:mm",
              }}
              format="YYYY-MM-DD HH:mm"
              onChange={onChange}
              onOk={onOk}
            />
          </Space>
        </div>
        <Table dataSource={dataSource} columns={columns} pagination={false} />
        <Pagination
          total={85}
          showSizeChanger
          showQuickJumper
          onChange={onPage}
          showTotal={(total) => `Total ${total} items`}
        />
      </Card>
    </Container>
  );
}
export default Orders;
