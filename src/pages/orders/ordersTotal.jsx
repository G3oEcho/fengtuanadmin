//导入样式
import { Container } from "./styled.orderTotal";
//导入api

//导入组件
// import * as echarts from "echarts";
import LineBarChart from "./components/LineBarChart";
import { Card } from "antd";
import {} from "@ant-design/icons";
// import React, { useEffect, useRef } from "react";
function OrdersTotal() {
  // const chartRef = useRef(null);
  return (
    <Container>
      <Card title="订单统计">
        <div className="total">
          <div className="box">
            <div className="order_name">本月销售量</div>
            <div className="order_num">
              <span>32</span>
            </div>
          </div>
          <div className="box">
            <div className="order_name">上月销售量</div>
            <div className="order_num">
              <span>34</span>
            </div>
          </div>
          <div className="box">
            <div className="order_name">总库存</div>
            <div className="order_num">
              <span>21,474,044,918</span>
            </div>
          </div>
          <div className="box">
            <div className="order_name">本月新注册用户</div>
            <div className="order_num">
              <span>113</span>
            </div>
          </div>
        </div>
        <div className="middle">
          <LineBarChart></LineBarChart>
        </div>
      </Card>
    </Container>
  );
}
export default OrdersTotal;
