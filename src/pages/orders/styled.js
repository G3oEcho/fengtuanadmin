import styled from "styled-components";

export const Container = styled.div`
  .title {
    display: flex;
  }
  .ant-table {
    margin-top: 20px;
  }
  .ant-pagination {
    margin-top: 40px;
    text-align: center;
  }
`;
